<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Course</title>
  </head>
  <body>
    <h1>Course</h1>
    <ul>
      <?php
        include "recup_liste.php";
        foreach ($liste as $key => $value):
          ?>
            <li id="<?php echo $value['id']; ?>">
              <?php echo $value['val']; ?>
              <small>
                <a href="supprimer.php?id=<?php echo $value['id']; ?>">supprimer</a>
              </small>
            </li>
          <?php
        endforeach;
       ?>
    </ul>
    <form action="ajouter.php" method="post">
      <input type="text" name="nom_nouv_prod">
      <input type="submit" value="Ajouter">
    </form>
  </body>
</html>
