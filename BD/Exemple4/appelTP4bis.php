<?php
	echo'POST : ';print_r($_POST);echo'<br/>';
	echo'GET : ';print_r($_GET);echo'<br/>';
	echo'URL : ';print_r($_SERVER['PHP_SELF']);echo'<br/>';  
	echo '<h1>Principes de l\'exercice</h1>';
	echo '
		Formulaire de saisie. Methode  GET.  
		<br>Where realisateur like :realisateur and annee <= :annee
		<br>Faille XSS gérée via un prepare() execute().
		<br>Ajout d\'un or die sur le execute.
	';
	
	echo '<h1>Résultats</h1>';;

	// connexion à la BD
	include("connexion.php");
	$bdd=connexionBD('cinema');	

	// reqSQL : version avec :realisateur au lieu de ?		
	echo '<h2>  On peut tester directement dans la ligne d\'URL</h2>';
//		$reqSQL='SELECT * FROM films WHERE realisateur like ? and annee < ? order by realisateur, annee';
	$reqSQL='SELECT * FROM films WHERE realisateur like :realisateur and annee <= :annee order by realisateur, annee';
	echo $reqSQL.'<br/>';

	// prepare et execute : version :realisateur dans le reqSQL	
	$requete=$bdd->prepare($reqSQL);
	$requete->execute(array(
		'realisateur' => $_GET['realisateur'],
		'annee'       => $_GET['annee']
	))
	or die(print_r($bdd->errorInfo())); 
	// or die : pour avoir des messages plus clairs en phase de dév
	// inutile avec une connexion en ERRMODE (ce qui est le cas avec notre fonction de connextion)
	
	// affichage des résultats
	echo '<h2>  Affichage des résultats</h2>';
	print_r($requete);
	$cpt=1;
	echo '<ul>';
	while ($ligne = $requete->fetch()) {
		echo '<li>[' .$cpt++. '] ' .$ligne['realisateur']. ' : ' .$ligne['annee']. ' - ' .$ligne['titre']. '</li>';
	}
	echo '</ul>';
	if($cpt==1) echo '<h3>aucun tuples trouvés<h3>';

	// fermeture préférable pour exécuter une nouvelle requete
	$requete->closeCursor();

	echo '<h1>Fin</h1>';
?>
