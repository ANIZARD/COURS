<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Test de $_GET sans faille v2</title>
	</head>
	
	<body>
		<h1>Test de $_GET - realisateur = :realisateur - Sans faille XSS :  Woody Allen' or 'a'='a</h1>
		<form action="appelTP3bis.php" method="GET" >
			<p><label for="realisateur">nom</label>
			<input type="text" name="realisateur" id="realisateur" placeholder="realisateur"></p>
		  <p><input type="submit" value="Valider"></p>
		</form>	
	</body>	
</html>