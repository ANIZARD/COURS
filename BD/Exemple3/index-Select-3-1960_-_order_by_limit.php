<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Test de SELECT</title>
	</head>
	
	<body>
		<?php
		include("connexion.php");
		$bdd=connexionBD('cinema');		

		$reqSQL='
			SELECT annee, titre, realisateur 
			FROM films 
			where annee=1960 
			order by titre LIMIT 0, 10
		';
		$requete=$bdd->query($reqSQL);
		
		$cpt=1;
		echo '<p><strong> Réalisateur : Titre - Annéee</strong>';
		while($ligne=$requete->fetch()){
			echo '<br/>[' .$cpt++. '] : ' .$ligne['annee']. ' : ' .$ligne['titre']. ' - ' .$ligne['realisateur'];
		}
		echo '</p>';

		$requete->closeCursor(); // pour finir le traitement

		?>

		<h1> FIN </h1>
	</body>	
</html>