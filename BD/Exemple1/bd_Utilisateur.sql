-- DROP DATABASE IF EXISTS BDUtilisateur;
-- CREATE DATABASE BD_Utilisateur;
-- USE BD_Utilisateur;

CREATE TABLE Utilisateur (
  id int(11) primary key AUTO_INCREMENT,
  prenomNom varchar(20) NOT NULL,
  adMail varchar(20) NOT NULL,
  motDePasse varchar(20) NOT NULL,
  annee int(4) NOT NULL
) ENGINE=InnoDB;

Insert into Utilisateur values (NULL, 'Sia PEI',     'ji@gmail.com','jipei', 1995);
Insert into Utilisateur values (NULL, 'Yawei CAI',   'jawei@yahoo.com','yaweicai',1996);
Insert into Utilisateur values (NULL, 'Zikeng PENG', 'zikeng@china.com','zikeng',1994);
Insert into Utilisateur values (NULL, 'Jiawen LI',   'jiawen@orange.fr','jiawen',1995);
Insert into Utilisateur values (NULL, 'Xiaoyu LIU',  'xiowyu@gmail.fr','liu',1996);
Insert into Utilisateur values (NULL, 'Olivier TRAN','tran@gmailcom','olivier',1997);
