<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>INTERFACE VUE.JS et BOOTSTRAP</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

     </head>

  <body>
    <div id="app">
      {{ user }}
        <div class="container">
          <div class="row">
            <p class="jumbotron">login: Vous êtes connecté en tant que {{ user }}</p>
          </div>
        </div>
    </div>

    <script>
        var app = new Vue({

            el: "#app",
            data: {
              user: "admin"
            }
        })
    </script>


  </body>
</html>